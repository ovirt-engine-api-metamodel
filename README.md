# oVirt Engine API Metamodel moved to GitHub!

This repo on Gerrit is not used anymore.
Please use https://github.com/ovirt/ovirt-engine-api-metamodel

Thanks
